#!/bin/bash

echo 正在复制程序文件...
sudo mkdir /opt
sudo cp -r ./bin/* /opt/

echo 正在安装字体...
sudo cp -r ./fonts/* /usr/share/fonts/truetype/
sudo mkfontscale /usr/share/fonts/truetype/ancient-scripts
sudo mkfontscale /usr/share/fonts/truetype/deepin
sudo mkfontscale /usr/share/fonts/truetype/unifont
sudo mkfontscale /usr/share/fonts/truetype/wqy
sudo fc-cache

echo 正在创建快捷方式...
sudo ln -s /opt/apps/com.qq.im.deepin/entries/com.qq.im.deepin.desktop /usr/share/applications/QQ.desktop
