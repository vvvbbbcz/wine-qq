# WineQQ For Linux

> 声明：此仓库内容并非由我开发，是直接从Deepin系统，版权由深度操作系统开发人员所有，使用此仓库内容所造成的任何后果由使用者承担。若本仓库内容侵犯了您的法律权益，请联系[vvvbbbcz@163.com](mailto:vvvbbbcz@163.com)，我会立即删除相关内容。
>
> 向深度操作系统开发人员致以崇高的敬意！

---

## 引子

Linux上的QQ一直是痛点。原生Linux的QQ功能不够完善，使用传统wine的QQ或是卡慢，或是不稳定。虽然Deepin上的WineQQ还可以，但人们不可能仅仅为了使用QQ而安装Deepin。因此我也想尽全力，在我所使用的系统(Kubuntu)上安装Deepin-wineQQ。

我用了很多方法，例如Docker、或者使用deepin-wine5安装QQ，但效果并不好，或者是需要折腾。我是不善于折腾的人，因此我便想，有没有更简便的方法？在虚拟机运行Deepin，查找效果不好的原因时，突然想到：如果我把Deepin上的QQ复制下来，再扔到我的Kubuntu上，然后加个`.desktop`文件，岂不美哉？因此我立马开始了尝试，结果当然如我所愿——我成功了。

## 使用方法

1. 将本仓库克隆 (`git clone https://gitee.com/vvvbbbcz/wine-qq.git`) 或[下载压缩包](https://gitee.com/vvvbbbcz/wine-qq/repository/archive/master.zip)。
2. 解压并将`install.sh`授予可执行权限 (`chmod +x install.sh`)。
3. 以root权限运行`install.sh`文件 (`sudo ./install.sh`)。
4. 如果安装后，点击图标无反应，需要先运行`install-env.sh`文件 (`sudo ./install-env.sh`)，以安装`deepin-wine5`。