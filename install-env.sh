#!/bin/bash

echo ===============
echo 正在添加软件源...
echo ===============
sudo cp ./list/deepin-source.list /etc/apt/sources.list.d/

echo ===============
echo 正在添加密钥...
echo ===============
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 1C30362C0A53D5BB
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 78BD65473CB3BD13
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 425956BB3E31DF51

echo ===============
echo 更新软件包列表...
echo ===============
sudo apt update

echo ===============
echo 安装deepin-wine5...
echo ===============
sudo apt install -t focal deepin-wine5 deepin-wine5-i386

echo ===============
echo 删除软件源...
echo ===============
sudo rm /etc/apt/sources.list.d/deepin-source.list
sudo apt update
